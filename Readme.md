# TOC
1. [Delete *ALL* LFS files in repo](#markdown-header-delete-all-lfs-files-in-repo)
1. [Calculate Total LFS Files Size & Display top 5 largest LFS files in repo](#markdown-header-calculate-total-lfs-file-size-and-display-top-5-largest-lfs-files-in-repo)

## Disclaimer
This tool was NOT written by Atlassian developers and is considered a third-party tool. This means that this is also NOT supported by Atlassian. We highly recommend you have your team review the script before running it to ensure you understand the steps and actions taking place, as Atlassian is not responsible for the resulting configuration.

## Delete ALL LFS files in repo
***
#### File : [script_LFS-size.sh](scripts/count-lfs-size.sh)

    Desc : Used to delete all LFS files from LFS storage in Media using an API. 
    (i) Note this does not delete LFS Pointer files
    
    Requirement :
    - All Object IDs (OIDs) in a text file separated by line
        * Should not have extra characters. Just the OID
    - Replace the following Variables in the script
        * file="<path to text file>"
        * workspaceid="workspace_id"
        * repo_slug="repo_slug"
        * credential="<username:app_password>"

#### Usage : 
```
chmod +x scripts/lfs-storage-deletion.sh
./scripts/lfs-storage-deletion.sh
```

![Sample Screenshot](image/delete-lfs-files.png)
========
## Calculate Total LFS File Size and Display top 5 largest LFS files in repo
***
#### File : [script_LFS-size.sh](scripts/count-lfs-size.sh)
    Desc : Used to calculate size of all LFS files from the repository

#### Usage : 

```
chmod +x scripts/count-lfs-size.sh
./scripts/count-lfs-size.sh <path to repo>
```

Sample Usage : 

![Sample Screenshot](image/count-lfs-size-script-image.png)


#!/bin/bash

# 1 byte = 0.001 KB       | 1 KB = 1000 Bytes
# 1 byte = 0.000001 MB    | 1 MB = 1000000 Bytes
# 1 byte = 0.000000001 GB | 1 GB = 1,000000000 Bytes


# $1 is variable input of repo path when script is ran
RepoPath=$1
#curdir=$(pwd)

cd $RepoPath

#git lfs ls-files -d | grep size:
#git lfs ls-files --debug | grep size | awk 'FNR >= 1 {print $2}' | awk '{n += $1}; END{print n}'
#Grabbing LFS size and input into a  "temporary file"
git lfs ls-files --debug --all | grep size | awk 'FNR >= 1 {print $2}' > .tmp_lfs_size


#Reading sizes line by line and totaling it up.
total=0

while read i
do
    ((total += i))

done < .tmp_lfs_size

#Remove temporarily created file 
rm .tmp_lfs_size

echo ""

#echo "Converting to KB"

#kb=$(echo "scale=2; $total*0.001" | bc)
kb=$(printf "%.2f" $(echo "scale=2; $total*0.001" | bc))

#echo "Converting to MB"

#mb=$(echo "scale=2; $total*0.000001" | bc)
mb=$(printf "%.2f" $(echo "scale=2; $total*0.000001" | bc))

#echo "Converting to GB"

#gb=$(echo "scale=2;$total*0.000000001" | bc)
gb=$(printf "%.2f" $(echo "scale=2;$total*0.000000001" | bc))


#DISABLED - As Pipelines Does not support interactive UI
#ColorCoding TPUT 
#title_hlight=`tput setab 4`
#txt_bold=`tput bold`
#RED_FG=`tput setaf 4`
#GREEN_BG=`tput setab 2`
#RESET=`tput sgr0`


#echo ${title_hlight}"The Total LFS Size Consumed by this Repository:"${RESET}
#echo "Bytes ="${RED_FG} ${txt_bold} $total ${RESET}
#echo "Kilobytes ="${RED_FG} ${txt_bold} $kb ${RESET}
#echo "MegaBytes ="${RED_FG} ${txt_bold} $mb ${RESET}
#echo "Gigabytes ="${RED_FG} ${txt_bold} $gb ${RESET}

echo "The Total LFS Size Consumed by this Repository:"
echo "Bytes ="$total
echo "Kilobytes ="$kb
echo "MegaBytes ="$mb
echo "Gigabytes ="$gb
echo ""

echo "Top 5 Largest (Bytes) LFS File in repo :"

git lfs ls-files --debug --all | grep -e "filepath" -e "size" | awk -F: '{ print $2 }' | awk '!(NR%2){print$0p}{p=$0}' | sort -nr | head -5
echo ""

#cd $curdir
#!/bin/bash

#  Get list of OID through pointer file in repo 
### git lfs ls-files --debug | grep oid  | awk 'FNR >= 1 {print $3}'


file="<path to text file>"
workspaceid="workspace_id"
repo_slug="repo_slug"
credential="<username:app_password>"

i=0

while read line || [ -n "$line" ]
do 
	i=$((i+1))
	echo "File number : $i"
	echo "LFS Object ID (OID) : $line" 
	cmd=$(curl -s -w "\nHTTP Code:  %{http_code}\n" --user $credential -X DELETE "https://api.bitbucket.org/internal/repositories/$workspaceid/$repo_slug/lfs/$line")
	echo "Response : $cmd"
	echo ""
done < $file

echo ""
echo "Total LFS Object ID in $file :" 
grep -c "" $file